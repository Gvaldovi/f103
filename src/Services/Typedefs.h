/****************************************************************************************************/
/**
  \file         Typedefs.h
  \brief        
  \author       Gerardo Valdovinos
  \project      IKMaster
  \version      
  \date         Aug 8, 2016

  ESTE DOCUMENTO Y SU CONTENIDO ES PROPIEDAD DE SIREMCO S.A.P.I. DE C.V. LAS COPIAS IMPRESAS DE ESTE
  DOCUMENTO SON COPIAS NO CONTROLADAS. PROHIBIDA SU REPRODUCCION PARCIAL O TOTAL SIN CONSENTIMIENTO
  DE SIREMCO S.A.P.I. DE C.V. SIREMCO(R) - CONFIDENCIAL -
*/
/****************************************************************************************************/
#ifndef __TIPEDEFS_H
#define	__TIPEDEFS_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
/* STM32 Library includes. */
#include "stm32f10x.h"
#include "stm32f10x_exti.h"
#include <stddef.h>
#include <string.h>
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/
#ifndef ON
        #define    ON     1u
#endif

#ifndef OFF
        #define    OFF    0u
#endif

#ifndef TRUE
        #define    TRUE   1u
#endif

#ifndef FALSE
        #define    FALSE  0u
#endif

#ifndef PASS
        #define    PASS   0u
#endif

#ifndef FAIL
        #define    FAIL   1u
#endif

#ifndef SETED
        #define    SETED    1u
#endif

#ifndef CLEAR
        #define    CLEAR  0u
#endif

#ifndef YES
        #define    YES    1u
#endif

#ifndef NO
        #define    NO     0u
#endif

#ifndef DISABLED
        #define          DISABLED  0u
#endif

#ifndef ENABLED
        #define          ENABLED  1u
#endif

#ifndef LOCK
        #define    LOCK     1u
#endif

#ifndef UNLOCK
        #define    UNLOCK    0u
#endif

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
typedef  void  (* vpfn )( void );
typedef  void  (* u8pfn )( u8 );
typedef unsigned char BYTE;
typedef unsigned short WORD;
/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
#endif	/* __TIPEDEFS_H */
/***************************************End of File**************************************************/
