/****************************************************************************************************/
/**
  \file         Utility.c
  \brief        Utilities as CRC, global const, errors definitions, address of external memories.
  \author       Gerardo Valdovinos
  \project      IKMaster1.0
  \version      0.0.1
  \date         Apr 9, 2015

  Program compiled with "COMPILER VERSION",
  Tested on "IK MASTER" board.

  ESTE DOCUMENTO Y SU CONTENIDO ES PROPIEDAD DE SIREMCO S.A.P.I. DE C.V. LAS COPIAS IMPRESAS DE ESTE
  DOCUMENTO SON COPIAS NO CONTROLADAS. PROHIBIDA SU REPRODUCCION PARCIAL O TOTAL SIN CONSENTIMIENTO
  DE SIREMCO S.A.P.I. DE C.V. SIREMCO(R) - CONFIDENCIAL -
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
#include "Utility.h"

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                       Variables EXTERN                                             *
*****************************************************************************************************/
/* External RTOS objects */
//extern TaskHandle_t xTask1T;
//extern TaskHandle_t xTask2T;
//extern TaskHandle_t xTask3T;
/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/
static u32 lut[16] =
{
	0x00000000,0x1DB71064,0x3B6E20C8,0x26D930AC,0x76DC4190,0x6B6B51F4,0x4DB26158,0x5005713C,
	0xEDB88320,0xF00F9344,0xD6D6A3E8,0xCB61B38C,0x9B64C2B0,0x86D3D2D4,0xA00AE278,0xBDBDF21C
};

/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of FUNCTIONS                                         *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Calculates the CRC 16 of a block of data using LUTs
* \author   Americo Lorenzana
* \param    BYTE* ptFRAME - Block starting address
* \param    WORD wSizeFrame -  Block size
* \param    WORD wCRCStart - Frame to start to calculate default 0xFFFF
* \return   CRC value (u16)
*****************************************************************************************************/
u16 u16fnUtilityCrc (u8 *ptFRAME, u16 wSizeFrame, u16 wCRCStart)
{
    u16 temp;
    u8 c, flag;
    for (;wSizeFrame>0;wSizeFrame--){
        temp = (u16) *ptFRAME;
        temp &= 0x00FF;
        wCRCStart = wCRCStart ^ temp;
        for (c=0;c<8;c++){
            flag = wCRCStart & 0x01;
            wCRCStart = wCRCStart>>1;
            if(flag != 0) wCRCStart = wCRCStart ^ 0xA001;
        }
        ptFRAME++;
    }
    //wCRCStart = (wCRCStart >> 8) | (wCRCStart << 8);
    return (wCRCStart);
}

/*****************************************************************************************************
**
 * \brief    UINT32 u32fnCRC32halfbyte(const void* data, UINT64 length, UINT32 previousCrc32) -
 * \author
 * \param    const void* data
 *           UINT64 length
 *           UINT32 previousCrc32
 * \return   UINT32 crc32
**
*****************************************************************************************************/
u32 u32fnUtilityCrc(const void* data, u16 length, u32 previousCrc32) // start previousCrc32 = 0x00000000
{
  u32 crc = ~previousCrc32;
  unsigned char* current = (unsigned char*) data;

  while (length--)
  {
    crc = lut[(crc ^  *current      ) & 0x0F] ^ (crc >> 4);
    crc = lut[(crc ^ (*current >> 4)) & 0x0F] ^ (crc >> 4);
    current++;
  }
  return ~crc;
}

/*****************************************************************************************************
* \brief	Form a cmd response.
* \author   Gerardo Valdovinos
* \param
* \return   void
*****************************************************************************************************/
u16 u16fnUtilityCmd(tsHeader *psHeader, teStatus eStatus, u16 u16Size, u8 *pu8Data)
{
	u16 u16Crc;
	u16 u16Index;
	u8 u8Temp;

	if(pu8Data != NULL)
	{
		/* Move data to right */
		memmove(&pu8Data[sizeof(tsHeader)], &pu8Data[0], u16Size);

		/* Form new Header */
		u8Temp = psHeader->u8Dest;
		psHeader->u8Dest = psHeader->u8Orig;
		psHeader->u8Orig = u8Temp;
//		psHeader->u8Cmd;
		psHeader->eStatus = eStatus;
		psHeader->u16Size = u16Size;

		/* Add header */
		u16Index = 0;
		memcpy(&pu8Data[u16Index], (u8*)psHeader, sizeof(tsHeader));
		u16Index += sizeof(tsHeader);

		/* Add size of data */
		u16Index += u16Size;

		/* Add CRC */
		u16Crc = u16fnUtilityCrc(&pu8Data[0], u16Index, 0xFFFF);
		pu8Data[u16Index++] = ((u16Crc & 0x00FF) >> 0);
		pu8Data[u16Index++] = ((u16Crc & 0xFF00) >> 8);

		return u16Index;
	}
	else
		return 0;
}

/*****************************************************************************************************
* \brief	Form a queue response.
* \author   Gerardo Valdovinos
* \param
* \return   void
*****************************************************************************************************/
void vfnUtilityMsg(tsMsg *psMsg, teFlags eFlag, teCmd eCmd)
{
	u8 u8Temp;

	/* Swich task orig and dest. Save flag. */
	u8Temp = psMsg->sInfo.eTaskOrig;
	psMsg->sInfo.eTaskOrig = psMsg->sInfo.eTaskDest;
	psMsg->sInfo.eTaskDest = u8Temp;
	psMsg->sInfo.eFlag = eFlag;
	psMsg->sInfo.eIntCmd = eCmd;
}

/*****************************************************************************************************
* \brief	Send queues.
* \author   Gerardo Valdovinos
* \param    void
* \return   int
*****************************************************************************************************/
teIntStatus efnUtilitySend(tsMsg *psMsg, QueueHandle_t xQueue, u32 u32Timeout, u8 u8ErrorCount)
{
	teIntStatus eIntStatus = eINT_STATUS_UNDEFINED;
	tsMsg sMsg;
	u8 i;

	/* Verify NULL pointer */
	if((psMsg == NULL) || (psMsg->pu8Data == NULL))
		return eINT_STATUS_ERROR_DATA_POINTER;

	/* Verify flags */
	if( (psMsg->sInfo.eFlag != eINT_FLAG_ACTION)	&&
		(psMsg->sInfo.eFlag != eINT_FLAG_EXECUTE)	&&
		(psMsg->sInfo.eFlag != eINT_FLAG_DATA))
		return eINT_STATUS_ERROR_ACTION;

	/* Try u8ErrorCount times */
	for(i = 0;i < u8ErrorCount;i++)
	{
		// TODO: validar si efectivamente se envio el mensaje a la cola
//		if(psMsg->sInfo.eTaskDest == eTASK2)
//			xQueueSend(xQueue2Q, psMsg, 0);
//		if(psMsg->sInfo.eTaskDest == eTASK3)
//			xQueueSend(xQueue3Q, psMsg, 0);

		if(psMsg->sInfo.eFlag == eINT_FLAG_ACTION)
		{
			eIntStatus = eINT_STATUS_OK;
			break;
		}
		else
		{
			if(pdTRUE == xQueueReceive(xQueue, &sMsg, pdMS_TO_TICKS(u32Timeout)))
			{
				if(sMsg.sInfo.eTaskDest == psMsg->sInfo.eTaskOrig)
				{
					// TODO: agregar que cuando haya un execute y la respuesta sea data entonces reencolar
					if(psMsg->sInfo.eFlag == eINT_FLAG_EXECUTE)
					{
						if(sMsg.sInfo.eFlag == eINT_FLAG_EXECUTE_OK)
							eIntStatus = eINT_STATUS_OK;
						else if(sMsg.sInfo.eFlag == eINT_FLAG_EXECUTE_NOK)
							eIntStatus = eINT_STATUS_ERROR_EXECUTE;
					}
					else if(psMsg->sInfo.eFlag == eINT_FLAG_DATA)
					{
						if(sMsg.sInfo.eFlag == eINT_FLAG_DATA_OK)
						{
							memcpy(&psMsg->pu8Data[0], &sMsg.pu8Data[0], sMsg.u16Size);
							psMsg->u16Size = sMsg.u16Size;
							eIntStatus = eINT_STATUS_OK;
						}
						else if(sMsg.sInfo.eFlag == eINT_FLAG_DATA_NOK)
							eIntStatus = eINT_STATUS_ERROR_DATA;
					}
					else
						eIntStatus = eINT_STATUS_ERROR_ALLOWED_ACTION;
					break;
				}
				else
				{
					/* Unexpected item, send it to queue's back */
					xQueueSendToBack(xQueue, &sMsg, 0);
					eIntStatus = eINT_STATUS_ERROR_BAD_TASK;
				}
			}
			else
				eIntStatus = eINT_STATUS_ERROR_TIMEOUT;
		}
	}

	return eIntStatus;
}
/***************************************End of Functions Definition**********************************/




